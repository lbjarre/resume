
from flask import Flask, render_template
from flask_frozen import Freezer
from ruamel.yaml import YAML

RESUME_DATA_FILE = 'static/data/resume.yml'
with open(RESUME_DATA_FILE, 'r') as f:
    resume_data = YAML().load(f.read())

app = Flask(__name__)
app.config['FREEZER_BASE_URL'] = 'https://lbjarre.gitlab.io/resume'
app.config['FREEZER_DESTINATION'] = 'public'
freezer = Freezer(app)

@app.cli.command()
def freeze():
    freezer.freeze()

@app.cli.command()
def serve():
    freezer.run()

@app.route('/')
def hw():
    return render_template('index.html', **resume_data)

if __name__ == '__main__':
    freezer.run(debug=True)

